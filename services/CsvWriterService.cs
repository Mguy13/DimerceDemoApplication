using System.Globalization;
using CsvHelper;

class CsvFileWriterService
{
    private TextWriter textWriter; 
    private CsvWriter csvWriter;

    public CsvFileWriterService(FileInfo fileInfo) {
        textWriter = new StreamWriter(fileInfo.FullName);
        
        csvWriter = new CsvWriter(textWriter, CultureInfo.InvariantCulture);
    }

    public void writeOutRow(OrderModel orderModel) {
        csvWriter.WriteField(orderModel.OrderId);
        csvWriter.WriteField(orderModel.OrderDescription);
        csvWriter.WriteField(orderModel.CustomerId);
        csvWriter.WriteField(orderModel.TotalPrice);

        csvWriter.Flush();
        csvWriter.NextRecord();
    }
}