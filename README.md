# Project Description

## Running the project

1. Comment out all the code in [review.cs](review.cs) file. Missing directives in that file will otherwise prevent the the program from running. And this file is not central to the project itself.
   
2. The easiest way to run this would be to use `dotnet run [args]`. If you want a more realistic use case, you use `dotnet publish` to create an executable binary and then execute that binary with the command-line args as normal.

## Infrastructure disclaimer
I come from a background of developing mobile applications in Dart/Flutter, with a different set of conventions (such for file naming etc). I have done some research into doing it and tried sticking to it.  But in the end, the architecture is still heavily influenced by practices in mobile development.

Additionally, I haven't focussed too much on Exception handling, since I feel that the scope of this assignment, is just a simple PoC. I do have some basic safeguards for input validation, and basic exception printing.

### Structure

#### Assets
There ae 2 JSON files in the `assets` directory. These can be used as input as cli-args.

#### Data modelling objects
1. [OrderDTO](data/dtos/OrderDTO.cs): Captures the raw data received from a JSON file.
2. [OrderModel](models/OrderModel.cs): Captures the usable part of the `OrderDTO` that will be written out to the CSV.

#### Services
1. [ApplicationConfigurationService](services/ApplicationConfigurationService.cs): Stores the configuration that the program ran with. Such as the command-line args passed (and hence the mode), the actual input data, and the ouput destination. Along with the file-existnece checks
2. [CsvWriterService](services/CsvWriterService.cs): Writes an order to a file.
3. Deserializing the JSON was handled by a package. Could have made this into a service ideally, for better scaling up.

#### Packages used
1. Newtonsoft.Json: For deserialzing JSON into constituent DTOs easily.
2. CsvHelper: To write out to a CSV file.

# Code review exercise
The answer to the review exercise can be found in [Problems.md](Problems.md) file.
