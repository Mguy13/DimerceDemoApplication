﻿using DTO;
using static Configuration.ApplicationConfigurationService;

namespace OrderParser
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var filesInfoConfig = Configuration.ApplicationConfigurationService.InitializeFrom(args);
                var csvFileWriter = new CsvFileWriterService(filesInfoConfig.OutputFile);

                foreach (var jsonFile in filesInfoConfig.JsonFilesInfo)
                {
                    // Could have easily been another service, but only do that when it needs to be scaled.
                    var orderDTO = Newtonsoft.Json.JsonConvert.DeserializeObject<OrderDto>(File.ReadAllText(jsonFile.FullName));

                    var orderModel = new OrderModel(orderDTO);

                    csvFileWriter.writeOutRow(orderModel);
                }
            }
            catch (DemoApplicationConfigurationException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}