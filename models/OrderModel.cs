using DTO;

public sealed class OrderModel {
    public long OrderId { get; set; }
    public string OrderDescription { get; set; }

    public long CustomerId { get; set; }

    public double TotalPrice { get; set; }

    public OrderModel(OrderDto orderDTO) {
        OrderId = orderDTO.OrderId;
        OrderDescription = orderDTO.Description.Trim();
        CustomerId = orderDTO.CustomerId;
        TotalPrice = orderDTO.Products.Select(product => product.Price).Sum();
    }
}