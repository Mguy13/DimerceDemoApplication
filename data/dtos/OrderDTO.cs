namespace DTO
{
    using Newtonsoft.Json;

    public partial class OrderDto
    {
        [JsonProperty("OrderId")]
        public virtual long OrderId { get; set; }

        [JsonProperty("Description")]
        public virtual string Description { get; set; }

        [JsonProperty("customer ID")]
        public virtual long CustomerId { get; set; }

        [JsonProperty("products")]
        public virtual Product[] Products { get; set; }
    }

    public partial class Product
    {
        [JsonProperty("ProductId")]
        public virtual string ProductId { get; set; }

        [JsonProperty("Description")]
        public virtual string Description { get; set; }

        [JsonProperty("Amount")]
        public virtual double Amount { get; set; }

        [JsonProperty("price")]
        public virtual double Price { get; set; }
    }
}
